<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Play Fizz Buzz</title>
        <link href="/css/main.css" rel="stylesheet">
    </head>
    <body>
    	<center>
        <h2>Play Fizz Buzz</h2>
        <form action="/form" method="post">
            Enter a number:<br>
            <input type="text" id="fbInput" name="fbInput" onkeypress="validate(event)" width="60px">
            <br><br>
            <#if search?? && search.fbResult?? >
        	Answer:<br>
        	${search.fbResult}<br><br>
	        </#if>
            <input type="submit" value="Submit" onClick="return fieldEmpty()">
        </form>
        </center>
        <script src="/js/main.js"></script>
    </body>
</html>