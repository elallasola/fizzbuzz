package project;

public final class SearchUtil {

	public static String fizzBuzz(int value) {
		String ret = "";
		if (value % 3 == 0 && value % 5 == 0) {
			if (value == 0) {
				ret = String.valueOf(value);
			} else {
				ret = "fizz buzz";
			}
		} else if (value % 3 == 0) {
			ret = "fizz";
		} else if (value % 5 == 0) {
			ret = "buzz";
		} else {
			ret = String.valueOf(value);
		}
		return ret;
	}
}
