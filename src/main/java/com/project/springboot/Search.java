package com.project.springboot;

public class Search {
    private int fbInput;
    private String fbResult;

    public int getFbInput() {
        return fbInput;
    }

    public void setFbInput(int fbInput) {
        this.fbInput = fbInput;
    }

    public String getFbResult() {
        return fbResult;
    }

    public void setFbResult(String fbResult) {
        this.fbResult = fbResult;
    }
}
