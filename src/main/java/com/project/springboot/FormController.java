package com.project.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import project.SearchUtil;

@Controller
public class FormController {
    
    @GetMapping("/")
    public String index() {
        return "redirect:/form";
    }

    @GetMapping("/form")
    public String formGet() {
        return "form";
    }

    @PostMapping("/form")
    public String formPost(Search search, Model model) {
    	if(search != null && search.getFbInput() >= 0) {
        	search.setFbResult(SearchUtil.fizzBuzz(search.getFbInput()));
    	}else {
    		search.setFbResult("Try again.");
    	}
        model.addAttribute("search", search);
        return "form";
    }
}
