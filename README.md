# FizzBuzz

## What you'll need
- JDK 1.8 or later
- Maven 3 or later

## Stack
- Java
- Spring Boot
- FreeMarker

## Run
`mvn spring-boot:run`
